import { gods } from "./arquivo_exercicio_4.js"

// ATRIBUIDO OS NOMES E AS CLASSES EM ARRAYS SEPARADOS
let names = []
gods.forEach((god) => {
    names.push(god.name)
})
let classesGods = []
gods.forEach((god) => {
    classesGods.push(god.class)
})

// JUNTADO OS ARRAYs EM UM NOVO ARRAY
let namesClasses = []
names.forEach((a,b) => {
    namesClasses.push(names[b]+"("+classesGods[b]+")" )
})

console.log(namesClasses)