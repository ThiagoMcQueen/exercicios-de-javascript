// gerando matriz aleatória
function MatrizGenerator(rows, columns){
    row = []
    matriz=[]    
    for(let i=0; i < rows; i++){
        //GERAÇÃO DE LINHA DE VALORES ALEATÓRIOS
        for(let j=0; j < columns; j++){
            number = Math.floor(Math.random()*10)// GERAÇÃO DE VALORES ALEATÓRIOS        
            row.push(number)// ATRIBUIÇÃO DO VALOR À LINHA  
        }
        matriz.push(row)// ATRIBUIÇÃO DA LINHA À MATRIZ
        row = []
    }
    return matriz    
}
// GERANDO MATRIX NULA C
function NullMatrizGenerator(rows, columns){
    row = []
    matriz=[]    
    for(let i=0; i < rows; i++){
        for(let j=0; j < columns; j++){
            number = 0
            row.push(number)    
        }
        matriz.push(row)
        row = []
    }
    return matriz    
}
//MULTIPLICAÇÃO DE MATRIZES
function MultiplicationMatriz(A, B){     
    for(let i=0; i < rowsA; i++){
        for(let j=0; j < columnsB; j++){
            for(let k=0; k < rowsB; k++){
                C[i][j] += A[i][k] * B[k][j]
            }
        }        
    }
    return C    
}
// DIMENÇÕES DAS MATRIZES
rowsA = 2
columnsA = 2
rowsB = columnsA
columnsB = 3
// CHAMADAS DA FUNÇÕES
A=MatrizGenerator(rowsA, columnsA)
B=MatrizGenerator(rowsA, columnsB)
C=NullMatrizGenerator(rowsA, columnsB)
C=MultiplicationMatriz(A, B)
// EXIBIÇÃO
console.log("Matriz A:")
for(let i=0; i < rowsA; i++){
    console.log(A[i])
}
console.log("Matriz B:")
for(let i=0; i < rowsB; i++){
    console.log(B[i])
}
console.log("Matriz C:")
for(let i=0; i < rowsA; i++){
    console.log(C[i])
}

